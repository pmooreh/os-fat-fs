#ifndef _FAT439_H_
#define _FAT439_H_

#include "fs.h"

/**************************/
/* The FAT439 file system */
/**************************/

struct SuperBlock {
    char magic[4];     // F439
    uint32_t nBlocks;  // total number of blocks: superblock + fat + data
    uint32_t avail;    // FAT index of the first available block
    uint32_t rootDir;  // FAT index of the root directory
};

struct Fat {
	size_t* blockList;
};

class Fat439 : public FileSystem {
public:
	StrongPtr<SuperBlock> superBlock;
	StrongPtr<Fat> fat;

    Fat439(StrongPtr<BlockIO> dev);
};

class FatFile : public File {
public:
	size_t startBlock;
	StrongPtr<Fat> fat;
	StrongPtr<BlockIO> dev;
	uint32_t type;
	uint32_t size;

	size_t traverseFat(size_t startBlock, size_t blockFileIndex);
    size_t nextInFat(size_t startBlock);

	FatFile(size_t blockNumber, StrongPtr<Fat> fat, StrongPtr<BlockIO> dev);

    uint32_t getType();
    uint32_t getLength();

    size_t read(size_t offset, void* buf, size_t nbyte);
    size_t readAll(size_t offset, void* buf, size_t nbyte);
};

class FatDirectory : public Directory, public FatFile {
	size_t lookupHelper(const char* name);
public:
	FatDirectory(size_t blockNumber, StrongPtr<Fat> fat, StrongPtr<BlockIO> dev);

	void entryName(size_t i, char* name);
	size_t nEntries();

	StrongPtr<File> lookupFile(const char* name);
    StrongPtr<Directory> lookupDirectory(const char *name); 
};

#endif
