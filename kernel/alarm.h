#ifndef _ALARM_H_
#define _ALARM_H_

#include "locks.h"
#include "stdint.h"
#include "refs.h"

class Alarm {

    static BlockingLock* lock;
    static Alarm *first;

    uint32_t at;
    StrongPtr<Event> event;
    Alarm *next;

    Alarm(uint32_t after, StrongPtr<Event> event);

public:

    // initilize alarm sub-system
    static void init(void);

    // schedule an alarm in "after" seconds
    static void schedule(uint32_t after, StrongPtr<Event> event);

    // check if any alarms need to fire
    static void check();

};

#endif
