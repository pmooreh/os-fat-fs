#define ASSEMBLY

	// int cmpxchg(int* ptr, int old, int new)
	.global cmpxchg
cmpxchg:
	mov 4(%esp),%ecx          // ptr
	mov 8(%esp),%eax          // old
	mov 12(%esp),%edx         // new
	lock cmpxchg %edx,(%ecx)
	ret

	// getThenIncrement(int* p, int d)
	.global getThenIncrement
getThenIncrement:
	mov 4(%esp),%edx
	mov 8(%esp),%eax
	lock xadd %eax,(%edx)
	ret

	.global cli
cli:
	pushf
	pop %eax
	cli
	ret

	.global sti
sti:
	pushf
	pop %eax
	sti
	ret

	# outb(int port, int val)
	.global outb
outb:
	push %edx
	mov 8(%esp),%dx
	mov 12(%esp),%al
	outb %al,%dx
	pop %edx
	ret

	# int inb(int port)
	.global inb
inb:
	push %edx
	mov 8(%esp),%dx
	inb %dx,%al
	pop %edx
	and $0xff,%eax
	ret

	# unsigned long inb(int port)
	.global inl
inl:
	push %edx
	mov 8(%esp),%dx
	inl %dx,%eax
	pop %edx
	ret

 /* pit_init(divide) */
        .global pit_do_init
pit_do_init:
        pushf                   # push IF
        cli                     # disable interrupts
        movb $0b00110100,%al    # 00 (channel 0)
                                # 110 (lobyte/hibyte)
                                # 100 (rate generator)
        outb %al,$0x43          # write command
        movb 8(%esp),%al        # divide
        outb %al,$0x40
        movb 9(%esp),%al
        outb %al,$0x40
        popf                    # pop IF
        ret


	.global pitAsmHandler
pitAsmHandler:
	pushf
	pusha

	.extern pitHandler
	call pitHandler

	popa
	popf
	iret



	.extern picHandler

	.global pic0
pic0:
	pusha
	mov $0,%eax
	push %eax
        call picHandler
	pop %eax

	popa
	iret

	.global pic1
pic1:
	pusha
	mov $1,%eax
	push %eax
        call picHandler
	pop %eax

	popa
	iret

	.global pic2
pic2:
	pusha
	push $2
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic3
pic3:
	pusha
	push $3
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic4
pic4:
	pusha
	push $4
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic5
pic5:
	pusha
	push $5
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic6
pic6:
	pusha
	push $6
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic7
pic7:
	pusha
	push $7
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic8
pic8:
	pusha
	push $8
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic9
pic9:
	pusha
	push $9
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic10
pic10:
	pusha
	push $10
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic11
pic11:
	pusha
	push $11
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic12
pic12:
	pusha
	push $12
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic13
pic13:
	pusha
	push $13
	.extern picHandler
        call picHandler
	pop %eax

	popa
	iret

	.global pic14
pic14:
#	pushf
#	pusha
#	mov $14,%eax
#	push %eax
#	.extern picHandler
#        call picHandler
#	pop %eax
#
#	popa
#	popf
	iret

	.global pic15
pic15:
#	pushf
#	pusha
#	mov $15,%eax
#	push %eax
#	.extern picHandler
#        call picHandler
#	pop %eax
#
#	popa
#	popf
	iret

