#include "pit.h"
#include "debug.h"
#include "machine.h"
#include "idt.h"
#include "pic.h"
#include "thread.h"
#include "refs.h"
#include "locks.h"
#include "alarm.h"

#define FREQ 1193182

#define IRQ 0

uint32_t Pit::ticksPerSecond;
uint32_t Pit::ticks;

void Pit::init(uint32_t hz) {

     idtAdd(picBase+IRQ, (uint32_t) pitAsmHandler);

     uint32_t d = FREQ / hz;

     Debug::printf("pitInit freq %dHz\n",hz);
     
     if ((d & 0xffff) != d) {
         Debug::printf("pitInit invalid divider %d\n",d);
         d = 0xffff;
     }
     Debug::printf("pitInit divider %d\n",d);
     ticksPerSecond = FREQ / d;
     Debug::printf("pitInit actual freq %dHz\n",ticksPerSecond);
     pit_do_init(d);
}

uint32_t Pit::secondsToTicks(uint32_t secs) {
    return secs * ticksPerSecond;
}

extern "C" void pitHandler() {
    Pit::ticks ++;
    picEoi(IRQ);
    Thread::yield();
}
