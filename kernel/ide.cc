#include "ide.h"
#include "ide_support.h"
#include "machine.h"
#include "stdint.h"
#include "idt.h"
#include "pic.h"

/*****************/
/* The IDE class */
/*****************/

static constexpr uint32_t SECTORS_PER_BLOCK = BlockDevice::BLOCK_SIZE / IDE_SECTOR_SIZE;

static StrongPtr<BlockIO> make(uint32_t drive) {
    return StrongPtr<BlockIO> { new IDE(drive) };
}

struct Info {
    StrongPtr<BlockIO> a;
    StrongPtr<BlockIO> b;
    StrongPtr<BlockIO> c;
    StrongPtr<BlockIO> d;

    BoundedBuffer<StrongPtr<BlockBuffer>> pending;

    Info() : a(make(0)), b(make(1)), c(make(2)), d(make(3)), pending(100) {
    }
};

static Info *info = nullptr;

StrongPtr<BlockIO> IDE::a() {
    return info->a;
}
    
StrongPtr<BlockIO> IDE::b() {
    return info->b;
}
    
StrongPtr<BlockIO> IDE::c() {
    return info->c;
}
    
StrongPtr<BlockIO> IDE::d() {
    return info->d;
}
    
static void ideThread(void) {
    while(true) {
        StrongPtr<BlockBuffer> buffer = info->pending.get();
        uint32_t id = buffer->id;

        uint32_t startingSector = buffer->blockNum * SECTORS_PER_BLOCK;
        char* ptr = buffer->data;

        for (uint32_t i = 0; i<SECTORS_PER_BLOCK; i++) {
            readSector(id, startingSector + i, (uintptr_t*) ptr);
            ptr += IDE_SECTOR_SIZE;
        }
        
        buffer->ready->signal();
    }
}

void IDE::init() {
    info = new Info();
    threadCreate(ideThread);
}

IDE::IDE(uint32_t drive) : BlockDevice(drive) {
}

StrongPtr<BlockBuffer> IDE::readBlock(size_t blockNum) {
    StrongPtr<BlockBuffer> buffer { new BlockBuffer(id,blockNum) };

    info->pending.put(buffer);

    return buffer;
}
