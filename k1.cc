#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "fat439.h"
#include "machine.h"
#include "thread.h"

volatile int* count;

void checkEqual(const char* str1, const char* str2) {
  char tmp;
    int j = 0;
    do {
      tmp = str2[j];

      if(str1[j] != tmp) {
        Debug::say("strings not equal: str1: %s str2: %s", str1, str2);
        return;
      }
      ++j;
    } while(tmp != 0);
}

void doStuff2(StrongPtr<Directory> root) {
  char name[13];
  root->entryName(0, name);
  StrongPtr<File> f = root->lookupFile(name);
  char buf[6];
  f->readAll(0, buf, 100);
  buf[5] = 0;
  checkEqual(buf, "hello");
  getThenIncrement(count, 1);
}

void doStuff(StrongPtr<Directory> root) {
  char name[13];
  char all[78];
  size_t i = 0;
  root->entryName(3, name); //big
  StrongPtr<File> f = root->lookupFile(name);
  size_t sz = f->getLength();
  for (size_t off = 0; off < sz; off += 8 * 10000) {
    char buf[7];
    f->readAll(off,buf,7);
    for(int j = 0; j < 7; j++) {
      all[j+i] = buf[j];
    }
    i+= 7;
  }

  all[77] = 0;
  checkEqual(all, "10000001010000102000010300001040000105000010600001070000108000010900001100000");
  getThenIncrement(count, 1);
}

void kernelMain(void) {

    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<Fat439> fs { new Fat439(d) };
    count = new int;
    StrongPtr<Directory> root = fs->rootdir;
    for(uint32_t i = 0; i < 50; ++i) {
      threadCreate<StrongPtr<Directory>>(doStuff, root);
      threadCreate<StrongPtr<Directory>>(doStuff2, root);
    }
    while(*count < 100) {
      Thread::yield();
    }
    delete count;
    Debug::say("yay");
    
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
}