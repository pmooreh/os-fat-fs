#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "fat439.h"

class LoopbackDev: public BlockDevice {
        StrongPtr<File> f;
public:
        LoopbackDev(uint32_t id, StrongPtr<File> f) : BlockDevice(id), f(f) {}
        virtual StrongPtr<BlockBuffer> readBlock(size_t blockNumber)
        {
                BlockBuffer *bb = new BlockBuffer(id, blockNumber);
                f->readAll(blockNumber * BlockIO::BLOCK_SIZE, bb->data, BlockIO::BLOCK_SIZE);
                bb->ready->signal();
                return StrongPtr<BlockBuffer>(bb);
        }
};

void ls(StrongPtr<Directory> d, const char *name)
{
        size_t n = d->nEntries();
        Debug::say("%d files in %s", n, name);
        for (size_t i = 0; i < n; i++) {
                char name[13];
                d->entryName(i, name);
                StrongPtr<File> f = d->lookupFile(name);
                size_t sz = f->getLength();
                Debug::say("[%d]  %7d  %s", i, sz, name);
                char *buf = new char[sz + 1];
                f->readAll(0, buf, sz);
                Debug::say("        %s",buf);
                delete[] buf;
        }
}
void kernelMain(void)
{
        StrongPtr<BlockIO> d = IDE::d();
        StrongPtr<Fat439> fs { new Fat439(d) };
        StrongPtr<Directory> root = fs->rootdir;
        ls(root, "root");
        Debug::printf("\n");
        Debug::say("mounting ryan.img");
        StrongPtr<BlockIO> loop = StrongPtr<BlockIO>(new LoopbackDev(42, root->lookupFile("ryan.img")));
        StrongPtr<Fat439> loopFs { new Fat439(loop) };
        StrongPtr<Directory> loopRoot = loopFs->rootdir;
        ls(loopRoot, "ryan.img");
}

/* Called when the system is about to shutdown */
void kernelTerminate() {}