#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "fat439.h"

void kernelMain(void) {

    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<Fat439> fs { new Fat439(d) };

    StrongPtr<Directory> root = fs->rootdir;

    size_t n = root->nEntries();
    Debug::say("root has %d entries",n);

    for (size_t i=0; i<n; i++) {
        char name[13];
        root->entryName(i,name);
        Debug::say("[%d] %s",i,name);
        StrongPtr<File> f = root->lookupFile(name);
        size_t sz = f->getLength();
        Debug::say("    type:%d length:%d",f->getType(),sz);

        if (sz > 20) {
            /* big file */
           for (size_t off = 24; off < sz; off += 8 * 5000) {
               char buf[8];
               f->readAll(off,buf,7);
               buf[7] = 0;
               Debug::say("            %s",buf);
           }
        } else {
            char *buf = new char[sz+1];
            buf[sz] = 0;
            size_t a = f->readAll(0,buf,sz + 1000000);
            Debug::say("        read %d bytes",a);
            Debug::say("            %s",buf);
            delete[] buf;
       }
    }
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
}
